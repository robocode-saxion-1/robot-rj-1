package ehi1vsc1_rikkertjan;

import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;

import static robocode.util.Utils.normalRelativeAngleDegrees;

/**
 * Created by Rikkert-Jan on 17-2-2016.
 */
public class EC1_RikkertJan extends AdvancedRobot{

    private boolean enemyInSight = false;
    private double gunTurningRate = 10;

    public void run()
    {
        setAdjustGunForRobotTurn(true);

        while (true)
        {
            turnGunRight(gunTurningRate);
        }
    }

    public void onScannedRobot(ScannedRobotEvent event)
    {
        setTurnRight(event.getBearing());
        setAhead(event.getDistance() - 200);
        gunTurningRate = normalRelativeAngleDegrees(event.getBearing() + (getHeading() - getRadarHeading()));

        setTurnGunRight(gunTurningRate);
        setTurnGunLeft(1);
        setFire(1);
    }

}
